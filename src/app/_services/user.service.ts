import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { environment } from "../../environments/environment";
import { User } from "../_models";
import { Response} from "../_models";

const baseUrl = `${environment.apiUrl}/users`;

@Injectable({ providedIn: "root" })
export class UserService {
  
  constructor(private http: HttpClient) {}

  getAll() {
    return this.http.get<User[]>(`${baseUrl}/getAll`);
  }

  getById(id: string) {
    return this.http.get<User>(`${baseUrl}/findById/${id}`);
  }

  create(params: any) {
    return this.http.post<Response>(`${baseUrl}/create`, params);
  }

  update(id: string, params: any) {
    return this.http.put(`${baseUrl}/update/${id}`, params);
  }

  delete(id: string) {
    return this.http.delete(`${baseUrl}/delete/${id}`);
  }
}
