

export class User {
    id!:  string;
    Nombres!: string;
    Apellidos!: string;
    Cedula!: string;
    Correo!: string;
    Telefono!: string;
    isDeleting: boolean = false;
}

